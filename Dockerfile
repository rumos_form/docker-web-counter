FROM python:3.10-slim-buster
LABEL description="python_webcounter"
LABEL maintainer="cfreire@cfreire.com.pt"
ARG REDIS_URL='localhost'
ENV REDIS_URL=${REDIS_URL}
WORKDIR /code
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
EXPOSE 5000
COPY . .
CMD ["python", "app.py"]
